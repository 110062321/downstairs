
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Platform.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a3e6dBne/JI4rscTjPZ8hyF', 'Platform');
// Scripts/Platform.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Platform = /** @class */ (function (_super) {
    __extends(Platform, _super);
    function Platform() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.soundEffect = null;
        _this.isTouched = false;
        _this.anim = null;
        _this.animState = null;
        _this.highestPos = 118;
        _this.moveSpeed = 100;
        _this.springVelocity = 320;
        return _this;
        //
        // 2. There are five different types of platforms: "Normal", "Fake", "Nails", "Trampoline", "Conveyor".
        //    When player touches the platform, you need to play the corresponding
        //    sound effect for each platform. (The audioClip named "soundEffect")
        //    Note that the sound effect only plays on the first time the player touches the platform.
        //
        // 3. "Trampoline" and "Fake" need to play animation when the player touches them.
        //    TAs have finished the animation functions, "playAnim" & "getAnimState", for you.
        //    You can directly call "playAnim" to play animation, and call "getAnimState" to get the current animation state.
        //    You have to play animations at the proper timing.
        //
        // 4. For "Trampoline", you have to do "spring effect" whenever the player touches it
        //
        //    Hints: Change "linearVelocity" of the player's rigidbody to make him jump.
        //    The jump value is "springVelocity".
        //
        // 5. For "Conveyor", you have to do "delivery effect" when player is in contact with it.
        //
        //    Hints: Change "linearVelocity" of the player's rigidbody to make him move.
        //    The move value is "moveSpeed".
        //
        // 6. For "Fake", you need to make the player fall 0.2 seconds after he touches the platform.
        //
        // 7. All the platforms have only "upside" collision. You have to prevent the collisions from the other directions.
        //
        //    Hints: You can use "contact.getWorldManifold().normal" to judge collision direction.
        //
        //
        // 8. When player touches "Nails" platform, you need to call the function "playerDamage" in "Player.ts" to update player health,
        //    or call the function "playerRecover" in "Player.ts" when player touches other platforms.
        //
        // 9. When platforms touch the node named "upperBound", you need to call the function "platformDestroy" to destroy the platform.
        // ================================================
    }
    Platform.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        if (this.node.name == "Conveyor") {
            this.node.scaleX = (Math.random() >= 0.5) ? 1 : -1;
            this.moveSpeed *= this.node.scaleX;
        }
    };
    Platform.prototype.reset = function () {
        this.isTouched = false;
    };
    Platform.prototype.update = function (dt) {
        if (this.node.y - this.highestPos >= 0 && this.node.y - this.highestPos < 100)
            this.getComponent(cc.PhysicsBoxCollider).enabled = false;
        else
            this.getComponent(cc.PhysicsBoxCollider).enabled = true;
    };
    Platform.prototype.playAnim = function () {
        if (this.anim)
            this.animState = this.anim.play();
    };
    Platform.prototype.getAnimState = function () {
        if (this.animState)
            return this.animState;
    };
    Platform.prototype.platformDestroy = function () {
        cc.log(this.node.name + " Platform destory.");
        this.node.destroy();
    };
    // ===================== TODO =====================
    // 1. In the physics lecture, we know that Cocos Creator
    //    provides four contact callbacks. You need to use callbacks to
    //    design different behaviors for different platforms.
    //
    //    Hints: The callbacks are "onBeginContact", "onEndContact", "onPreSolve", "onPostSolve".
    Platform.prototype.onBeginContact = function (contact, self, other) {
        var jump = 0;
        if (contact.getWorldManifold().normal.y < 0) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                if (!this.isTouched && this.soundEffect) {
                    jump = 0;
                    cc.audioEngine.playEffect(this.soundEffect, false);
                    other.node.getComponent("Player").playerRecover();
                    other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
                    this.isTouched = true;
                }
            }
            if (name == "Fake") {
                if (!this.isTouched && this.soundEffect) {
                    jump = 0;
                    cc.audioEngine.playEffect(this.soundEffect, false);
                    other.node.getComponent("Player").playerRecover();
                    this.scheduleOnce(function () {
                        contact.disabled = true;
                    }, 0.2);
                    this.isTouched = true;
                }
                this.playAnim();
            }
            if (name == "Nails") {
                if (!this.isTouched && this.soundEffect) {
                    jump = 0;
                    cc.audioEngine.playEffect(this.soundEffect, false);
                    other.node.getComponent("Player").playerDamage();
                    this.isTouched = true;
                }
            }
            if (name == "Trampoline") {
                cc.audioEngine.playEffect(this.soundEffect, false);
                other.node.getComponent("Player").playerRecover();
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.springVelocity);
                this.playAnim();
            }
            if (name == "Normal") {
                if (!this.isTouched && this.soundEffect) {
                    jump = 0;
                    cc.audioEngine.playEffect(this.soundEffect, false);
                    other.node.getComponent("Player").playerRecover();
                    this.isTouched = true;
                }
            }
            if (other.node.name == "upperBound") {
                this.platformDestroy();
            }
        }
    };
    Platform.prototype.onPreSolve = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 0) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
            }
        }
    };
    Platform.prototype.onPostSolve = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 0) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            }
        }
    };
    Platform.prototype.OnEndContact = function (contact, self, other) {
        if (contact.getWorldManifold().normal.y < 0) {
            contact.setEnabled(false);
        }
        else {
            var name = self.node.name;
            if (name == "Conveyor") {
                other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            }
        }
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], Platform.prototype, "soundEffect", void 0);
    Platform = __decorate([
        ccclass
    ], Platform);
    return Platform;
}(cc.Component));
exports.default = Platform;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcUGxhdGZvcm0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFNLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXNDLDRCQUFZO0lBQWxEO1FBQUEscUVBNk1DO1FBMU1HLGlCQUFXLEdBQWlCLElBQUksQ0FBQztRQUV2QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBRTdCLFVBQUksR0FBaUIsSUFBSSxDQUFDO1FBRTFCLGVBQVMsR0FBc0IsSUFBSSxDQUFDO1FBRXBDLGdCQUFVLEdBQVcsR0FBRyxDQUFDO1FBRXpCLGVBQVMsR0FBVyxHQUFHLENBQUM7UUFFeEIsb0JBQWMsR0FBVyxHQUFHLENBQUM7O1FBNEp2QyxFQUFFO1FBQ0YsdUdBQXVHO1FBQ3ZHLDBFQUEwRTtRQUMxRSx5RUFBeUU7UUFDekUsOEZBQThGO1FBQzlGLEVBQUU7UUFDRixrRkFBa0Y7UUFDbEYsc0ZBQXNGO1FBQ3RGLHFIQUFxSDtRQUNySCx1REFBdUQ7UUFDdkQsRUFBRTtRQUNGLHFGQUFxRjtRQUNyRixFQUFFO1FBQ0YsZ0ZBQWdGO1FBQ2hGLHlDQUF5QztRQUN6QyxFQUFFO1FBQ0YseUZBQXlGO1FBQ3pGLEVBQUU7UUFDRixnRkFBZ0Y7UUFDaEYsb0NBQW9DO1FBQ3BDLEVBQUU7UUFDRiw2RkFBNkY7UUFDN0YsRUFBRTtRQUNGLG1IQUFtSDtRQUNuSCxFQUFFO1FBQ0YsMEZBQTBGO1FBQzFGLEVBQUU7UUFDRixFQUFFO1FBQ0YsZ0lBQWdJO1FBQ2hJLDhGQUE4RjtRQUM5RixFQUFFO1FBQ0YsZ0lBQWdJO1FBQ2hJLG1EQUFtRDtJQUVyRCxDQUFDO0lBNUxHLHdCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRTVDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksVUFBVSxFQUFFO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7U0FDdEM7SUFDTCxDQUFDO0lBRUQsd0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRCx5QkFBTSxHQUFOLFVBQU8sRUFBRTtRQUNMLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHO1lBQ3pFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGtCQUFrQixDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzs7WUFFekQsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ2hFLENBQUM7SUFFRCwyQkFBUSxHQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsSUFBSTtZQUNULElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBRUQsK0JBQVksR0FBWjtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVM7WUFDZCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDOUIsQ0FBQztJQUVELGtDQUFlLEdBQWY7UUFFSSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLG9CQUFvQixDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsbURBQW1EO0lBQ3JELHdEQUF3RDtJQUN4RCxtRUFBbUU7SUFDbkUseURBQXlEO0lBQ3pELEVBQUU7SUFDRiw2RkFBNkY7SUFDM0YsaUNBQWMsR0FBZCxVQUFlLE9BQU8sRUFBQyxJQUFJLEVBQUMsS0FBSztRQUM3QixJQUFJLElBQUksR0FBRyxDQUFDLENBQUM7UUFDYixJQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0I7YUFDRztZQUNBLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBRTFCLElBQUcsSUFBSSxJQUFJLFVBQVUsRUFBQztnQkFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDckMsSUFBSSxHQUFHLENBQUMsQ0FBQztvQkFDVCxFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNuRCxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztvQkFDbEQsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2hGLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2lCQUN6QjthQUlKO1lBQ0QsSUFBRyxJQUFJLElBQUksTUFBTSxFQUFDO2dCQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ3JDLElBQUksR0FBRyxDQUFDLENBQUM7b0JBQ1QsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDbkQsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQ2xELElBQUksQ0FBQyxZQUFZLENBQUM7d0JBQ2QsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQzVCLENBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQztvQkFDUCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztpQkFDekI7Z0JBRUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBR25CO1lBQ0QsSUFBRyxJQUFJLElBQUksT0FBTyxFQUFDO2dCQUVmLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ3JDLElBQUksR0FBRyxDQUFDLENBQUM7b0JBQ1QsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDbkQsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ2pELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2lCQUN6QjthQUdKO1lBQ0QsSUFBRyxJQUFJLElBQUksWUFBWSxFQUFDO2dCQUVwQixFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNuRCxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDbEQsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3JGLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUluQjtZQUNELElBQUcsSUFBSSxJQUFJLFFBQVEsRUFBQztnQkFFaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDckMsSUFBSSxHQUFHLENBQUMsQ0FBQztvQkFDVCxFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNuRCxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztvQkFDbEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7aUJBQ3pCO2FBQ0o7WUFHRCxJQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBQztnQkFDL0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQzFCO1NBQ0o7SUFFTCxDQUFDO0lBRUQsNkJBQVUsR0FBVixVQUFXLE9BQU8sRUFBQyxJQUFJLEVBQUMsS0FBSztRQUN6QixJQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0I7YUFDRztZQUNBLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFCLElBQUcsSUFBSSxJQUFJLFVBQVUsRUFBQztnQkFDbEIsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDbkY7U0FDSjtJQUVMLENBQUM7SUFFRCw4QkFBVyxHQUFYLFVBQVksT0FBTyxFQUFDLElBQUksRUFBQyxLQUFLO1FBQzFCLElBQUcsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUM7WUFDdkMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM3QjthQUNHO1lBQ0EsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsSUFBRyxJQUFJLElBQUksVUFBVSxFQUFDO2dCQUNsQixLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3RFO1NBQ0o7SUFFTCxDQUFDO0lBRUQsK0JBQVksR0FBWixVQUFhLE9BQU8sRUFBQyxJQUFJLEVBQUMsS0FBSztRQUMzQixJQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0I7YUFDRztZQUNBLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFCLElBQUcsSUFBSSxJQUFJLFVBQVUsRUFBQztnQkFDbEIsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN0RTtTQUNKO0lBRUwsQ0FBQztJQXZLRDtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7aURBQ0E7SUFIaEIsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQTZNNUI7SUFBRCxlQUFDO0NBN01ELEFBNk1DLENBN01xQyxFQUFFLENBQUMsU0FBUyxHQTZNakQ7a0JBN01vQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFBsYXRmb3JtIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHJcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5BdWRpb0NsaXAgfSlcclxuICAgIHNvdW5kRWZmZWN0OiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG5cclxuICAgIHByb3RlY3RlZCBpc1RvdWNoZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBwcml2YXRlIGFuaW06IGNjLkFuaW1hdGlvbiA9IG51bGw7XHJcblxyXG4gICAgcHJpdmF0ZSBhbmltU3RhdGU6IGNjLkFuaW1hdGlvblN0YXRlID0gbnVsbDtcclxuXHJcbiAgICBwcml2YXRlIGhpZ2hlc3RQb3M6IG51bWJlciA9IDExODtcclxuXHJcbiAgICBwcml2YXRlIG1vdmVTcGVlZDogbnVtYmVyID0gMTAwO1xyXG5cclxuICAgIHByaXZhdGUgc3ByaW5nVmVsb2NpdHk6IG51bWJlciA9IDMyMDtcclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLmFuaW0gPSB0aGlzLmdldENvbXBvbmVudChjYy5BbmltYXRpb24pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5ub2RlLm5hbWUgPT0gXCJDb252ZXlvclwiKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZVggPSAoTWF0aC5yYW5kb20oKSA+PSAwLjUpID8gMSA6IC0xO1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVTcGVlZCAqPSB0aGlzLm5vZGUuc2NhbGVYO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXNldCgpIHtcclxuICAgICAgICB0aGlzLmlzVG91Y2hlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdCkge1xyXG4gICAgICAgIGlmICh0aGlzLm5vZGUueSAtIHRoaXMuaGlnaGVzdFBvcyA+PSAwICYmIHRoaXMubm9kZS55IC0gdGhpcy5oaWdoZXN0UG9zIDwgMTAwKVxyXG4gICAgICAgICAgICB0aGlzLmdldENvbXBvbmVudChjYy5QaHlzaWNzQm94Q29sbGlkZXIpLmVuYWJsZWQgPSBmYWxzZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlBoeXNpY3NCb3hDb2xsaWRlcikuZW5hYmxlZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcGxheUFuaW0oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYW5pbSlcclxuICAgICAgICAgICAgdGhpcy5hbmltU3RhdGUgPSB0aGlzLmFuaW0ucGxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFuaW1TdGF0ZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5hbmltU3RhdGUpXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFuaW1TdGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBwbGF0Zm9ybURlc3Ryb3koKVxyXG4gICAge1xyXG4gICAgICAgIGNjLmxvZyh0aGlzLm5vZGUubmFtZSArIFwiIFBsYXRmb3JtIGRlc3RvcnkuXCIpO1xyXG4gICAgICAgIHRoaXMubm9kZS5kZXN0cm95KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09IFRPRE8gPT09PT09PT09PT09PT09PT09PT09XHJcbiAgLy8gMS4gSW4gdGhlIHBoeXNpY3MgbGVjdHVyZSwgd2Uga25vdyB0aGF0IENvY29zIENyZWF0b3JcclxuICAvLyAgICBwcm92aWRlcyBmb3VyIGNvbnRhY3QgY2FsbGJhY2tzLiBZb3UgbmVlZCB0byB1c2UgY2FsbGJhY2tzIHRvXHJcbiAgLy8gICAgZGVzaWduIGRpZmZlcmVudCBiZWhhdmlvcnMgZm9yIGRpZmZlcmVudCBwbGF0Zm9ybXMuXHJcbiAgLy9cclxuICAvLyAgICBIaW50czogVGhlIGNhbGxiYWNrcyBhcmUgXCJvbkJlZ2luQ29udGFjdFwiLCBcIm9uRW5kQ29udGFjdFwiLCBcIm9uUHJlU29sdmVcIiwgXCJvblBvc3RTb2x2ZVwiLlxyXG4gICAgb25CZWdpbkNvbnRhY3QoY29udGFjdCxzZWxmLG90aGVyKXtcclxuICAgICAgICB2YXIganVtcCA9IDA7XHJcbiAgICAgICAgaWYoY29udGFjdC5nZXRXb3JsZE1hbmlmb2xkKCkubm9ybWFsLnkgPCAwKXtcclxuICAgICAgICAgICAgY29udGFjdC5zZXRFbmFibGVkKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgdmFyIG5hbWUgPSBzZWxmLm5vZGUubmFtZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgaWYobmFtZSA9PSBcIkNvbnZleW9yXCIpe1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmlzVG91Y2hlZCAmJiB0aGlzLnNvdW5kRWZmZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAganVtcCA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLnNvdW5kRWZmZWN0LCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoXCJQbGF5ZXJcIikucGxheWVyUmVjb3ZlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgIG90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keSkubGluZWFyVmVsb2NpdHkgPSBjYy52Mih0aGlzLm1vdmVTcGVlZCwgMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1RvdWNoZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYobmFtZSA9PSBcIkZha2VcIil7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNUb3VjaGVkICYmIHRoaXMuc291bmRFZmZlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICBqdW1wID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHRoaXMuc291bmRFZmZlY3QsIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvdGhlci5ub2RlLmdldENvbXBvbmVudChcIlBsYXllclwiKS5wbGF5ZXJSZWNvdmVyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGFjdC5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwwLjIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNUb3VjaGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wbGF5QW5pbSgpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYobmFtZSA9PSBcIk5haWxzXCIpe1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5pc1RvdWNoZWQgJiYgdGhpcy5zb3VuZEVmZmVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGp1bXAgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QodGhpcy5zb3VuZEVmZmVjdCwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KFwiUGxheWVyXCIpLnBsYXllckRhbWFnZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNUb3VjaGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKG5hbWUgPT0gXCJUcmFtcG9saW5lXCIpe1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QodGhpcy5zb3VuZEVmZmVjdCwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoXCJQbGF5ZXJcIikucGxheWVyUmVjb3ZlcigpO1xyXG4gICAgICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoY2MuUmlnaWRCb2R5KS5saW5lYXJWZWxvY2l0eSA9IGNjLnYyKDAsIHRoaXMuc3ByaW5nVmVsb2NpdHkpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wbGF5QW5pbSgpO1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZihuYW1lID09IFwiTm9ybWFsXCIpe1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5pc1RvdWNoZWQgJiYgdGhpcy5zb3VuZEVmZmVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGp1bXAgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QodGhpcy5zb3VuZEVmZmVjdCwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KFwiUGxheWVyXCIpLnBsYXllclJlY292ZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzVG91Y2hlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIGlmKG90aGVyLm5vZGUubmFtZSA9PSBcInVwcGVyQm91bmRcIil7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBsYXRmb3JtRGVzdHJveSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIG9uUHJlU29sdmUoY29udGFjdCxzZWxmLG90aGVyKXtcclxuICAgICAgICBpZihjb250YWN0LmdldFdvcmxkTWFuaWZvbGQoKS5ub3JtYWwueSA8IDApe1xyXG4gICAgICAgICAgICBjb250YWN0LnNldEVuYWJsZWQoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICB2YXIgbmFtZSA9IHNlbGYubm9kZS5uYW1lO1xyXG4gICAgICAgICAgICBpZihuYW1lID09IFwiQ29udmV5b3JcIil7XHJcbiAgICAgICAgICAgICAgICBvdGhlci5ub2RlLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIodGhpcy5tb3ZlU3BlZWQsIDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIG9uUG9zdFNvbHZlKGNvbnRhY3Qsc2VsZixvdGhlcil7XHJcbiAgICAgICAgaWYoY29udGFjdC5nZXRXb3JsZE1hbmlmb2xkKCkubm9ybWFsLnkgPCAwKXtcclxuICAgICAgICAgICAgY29udGFjdC5zZXRFbmFibGVkKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgdmFyIG5hbWUgPSBzZWxmLm5vZGUubmFtZTtcclxuICAgICAgICAgICAgaWYobmFtZSA9PSBcIkNvbnZleW9yXCIpe1xyXG4gICAgICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoY2MuUmlnaWRCb2R5KS5saW5lYXJWZWxvY2l0eSA9IGNjLnYyKDAsIDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIE9uRW5kQ29udGFjdChjb250YWN0LHNlbGYsb3RoZXIpe1xyXG4gICAgICAgIGlmKGNvbnRhY3QuZ2V0V29ybGRNYW5pZm9sZCgpLm5vcm1hbC55IDwgMCl7XHJcbiAgICAgICAgICAgIGNvbnRhY3Quc2V0RW5hYmxlZChmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIHZhciBuYW1lID0gc2VsZi5ub2RlLm5hbWU7XHJcbiAgICAgICAgICAgIGlmKG5hbWUgPT0gXCJDb252ZXlvclwiKXtcclxuICAgICAgICAgICAgICAgIG90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keSkubGluZWFyVmVsb2NpdHkgPSBjYy52MigwLCAwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgIH1cclxuICAvL1xyXG4gIC8vIDIuIFRoZXJlIGFyZSBmaXZlIGRpZmZlcmVudCB0eXBlcyBvZiBwbGF0Zm9ybXM6IFwiTm9ybWFsXCIsIFwiRmFrZVwiLCBcIk5haWxzXCIsIFwiVHJhbXBvbGluZVwiLCBcIkNvbnZleW9yXCIuXHJcbiAgLy8gICAgV2hlbiBwbGF5ZXIgdG91Y2hlcyB0aGUgcGxhdGZvcm0sIHlvdSBuZWVkIHRvIHBsYXkgdGhlIGNvcnJlc3BvbmRpbmdcclxuICAvLyAgICBzb3VuZCBlZmZlY3QgZm9yIGVhY2ggcGxhdGZvcm0uIChUaGUgYXVkaW9DbGlwIG5hbWVkIFwic291bmRFZmZlY3RcIilcclxuICAvLyAgICBOb3RlIHRoYXQgdGhlIHNvdW5kIGVmZmVjdCBvbmx5IHBsYXlzIG9uIHRoZSBmaXJzdCB0aW1lIHRoZSBwbGF5ZXIgdG91Y2hlcyB0aGUgcGxhdGZvcm0uXHJcbiAgLy9cclxuICAvLyAzLiBcIlRyYW1wb2xpbmVcIiBhbmQgXCJGYWtlXCIgbmVlZCB0byBwbGF5IGFuaW1hdGlvbiB3aGVuIHRoZSBwbGF5ZXIgdG91Y2hlcyB0aGVtLlxyXG4gIC8vICAgIFRBcyBoYXZlIGZpbmlzaGVkIHRoZSBhbmltYXRpb24gZnVuY3Rpb25zLCBcInBsYXlBbmltXCIgJiBcImdldEFuaW1TdGF0ZVwiLCBmb3IgeW91LlxyXG4gIC8vICAgIFlvdSBjYW4gZGlyZWN0bHkgY2FsbCBcInBsYXlBbmltXCIgdG8gcGxheSBhbmltYXRpb24sIGFuZCBjYWxsIFwiZ2V0QW5pbVN0YXRlXCIgdG8gZ2V0IHRoZSBjdXJyZW50IGFuaW1hdGlvbiBzdGF0ZS5cclxuICAvLyAgICBZb3UgaGF2ZSB0byBwbGF5IGFuaW1hdGlvbnMgYXQgdGhlIHByb3BlciB0aW1pbmcuXHJcbiAgLy9cclxuICAvLyA0LiBGb3IgXCJUcmFtcG9saW5lXCIsIHlvdSBoYXZlIHRvIGRvIFwic3ByaW5nIGVmZmVjdFwiIHdoZW5ldmVyIHRoZSBwbGF5ZXIgdG91Y2hlcyBpdFxyXG4gIC8vXHJcbiAgLy8gICAgSGludHM6IENoYW5nZSBcImxpbmVhclZlbG9jaXR5XCIgb2YgdGhlIHBsYXllcidzIHJpZ2lkYm9keSB0byBtYWtlIGhpbSBqdW1wLlxyXG4gIC8vICAgIFRoZSBqdW1wIHZhbHVlIGlzIFwic3ByaW5nVmVsb2NpdHlcIi5cclxuICAvL1xyXG4gIC8vIDUuIEZvciBcIkNvbnZleW9yXCIsIHlvdSBoYXZlIHRvIGRvIFwiZGVsaXZlcnkgZWZmZWN0XCIgd2hlbiBwbGF5ZXIgaXMgaW4gY29udGFjdCB3aXRoIGl0LlxyXG4gIC8vXHJcbiAgLy8gICAgSGludHM6IENoYW5nZSBcImxpbmVhclZlbG9jaXR5XCIgb2YgdGhlIHBsYXllcidzIHJpZ2lkYm9keSB0byBtYWtlIGhpbSBtb3ZlLlxyXG4gIC8vICAgIFRoZSBtb3ZlIHZhbHVlIGlzIFwibW92ZVNwZWVkXCIuXHJcbiAgLy9cclxuICAvLyA2LiBGb3IgXCJGYWtlXCIsIHlvdSBuZWVkIHRvIG1ha2UgdGhlIHBsYXllciBmYWxsIDAuMiBzZWNvbmRzIGFmdGVyIGhlIHRvdWNoZXMgdGhlIHBsYXRmb3JtLlxyXG4gIC8vXHJcbiAgLy8gNy4gQWxsIHRoZSBwbGF0Zm9ybXMgaGF2ZSBvbmx5IFwidXBzaWRlXCIgY29sbGlzaW9uLiBZb3UgaGF2ZSB0byBwcmV2ZW50IHRoZSBjb2xsaXNpb25zIGZyb20gdGhlIG90aGVyIGRpcmVjdGlvbnMuXHJcbiAgLy9cclxuICAvLyAgICBIaW50czogWW91IGNhbiB1c2UgXCJjb250YWN0LmdldFdvcmxkTWFuaWZvbGQoKS5ub3JtYWxcIiB0byBqdWRnZSBjb2xsaXNpb24gZGlyZWN0aW9uLlxyXG4gIC8vXHJcbiAgLy9cclxuICAvLyA4LiBXaGVuIHBsYXllciB0b3VjaGVzIFwiTmFpbHNcIiBwbGF0Zm9ybSwgeW91IG5lZWQgdG8gY2FsbCB0aGUgZnVuY3Rpb24gXCJwbGF5ZXJEYW1hZ2VcIiBpbiBcIlBsYXllci50c1wiIHRvIHVwZGF0ZSBwbGF5ZXIgaGVhbHRoLFxyXG4gIC8vICAgIG9yIGNhbGwgdGhlIGZ1bmN0aW9uIFwicGxheWVyUmVjb3ZlclwiIGluIFwiUGxheWVyLnRzXCIgd2hlbiBwbGF5ZXIgdG91Y2hlcyBvdGhlciBwbGF0Zm9ybXMuXHJcbiAgLy9cclxuICAvLyA5LiBXaGVuIHBsYXRmb3JtcyB0b3VjaCB0aGUgbm9kZSBuYW1lZCBcInVwcGVyQm91bmRcIiwgeW91IG5lZWQgdG8gY2FsbCB0aGUgZnVuY3Rpb24gXCJwbGF0Zm9ybURlc3Ryb3lcIiB0byBkZXN0cm95IHRoZSBwbGF0Zm9ybS5cclxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuXHJcbn1cclxuIl19